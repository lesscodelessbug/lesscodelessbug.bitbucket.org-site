# Introduction #

This project is a Pelican based static generator for generating and publishing blog 
content to http://lesscodelessbug.bitbucket.org. 

The published site is actually stored in 
https://bitbucket.org/lesscodelessbug/lesscodelessbug.bitbucket.org as separate repository.

# Setup Pelican #

## Install tuxlite_tbs theme
git clone http://github.com/getpelican/pelican-themes

## Install pelican
cd lesscodelessbug.bitbucket.org-site
python3 -m venv mypelicanenv
source mypelicanenv/bin/activate
pip install -r requirements.txt

# Create a New Article #

mkdir -p content/`date +%Y/%m`
touch tontent/`date +%Y/%m`/`date +%d`-newblog.md

# Generate Site #

source mypelicanenv/bin/activate

## Generate dev site (Just use browser to open output/index.html or python -m SimpleHTTPServer)
pelican --theme-path ../pelican-themes/tuxlite_tbs --autoreload

## Generate production ready site
pelican --theme-path ../pelican-themes/tuxlite_tbs --settings publishconf.py --output ../lesscodelessbug.bitbucket.org
