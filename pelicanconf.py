#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Zemian Deng'
SITENAME = 'Less Code Less Bug!'
SITEURL = 'http://localhost:8000'

PATH = 'content'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_ORDER_BY = 'reversed-date'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
         ('My Code @ BitBucket', 'https://bitbucket.org/zemian/'),
         ('My Code @ GitHub', 'https://github.com/zemian/'),
         ('My DZone', 'https://dzone.com/users/748493/saltnlight5.html'),
         ('My Old Blog', 'http://saltnlight5.blogspot.com/')
        )

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/zemiandeng'),
          ('LinkedIn', 'https://www.linkedin.com/in/zemian-deng-profile'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DEFAULT_DATE = 'fs'
DEFAULT_CATEGORY = 'Programming'
USE_FOLDER_AS_CATEGORY = False
#DISPLAY_PAGES_ON_MENU = False

THEME = 'tuxlite_tbs'
#CSS_FILE = 'wide.css'

MD_EXTENSIONS = ['codehilite(css_class=highlight)','extra', 'admonition', 'toc']
