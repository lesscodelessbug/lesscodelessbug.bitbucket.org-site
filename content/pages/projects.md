Title: Projects

---
# Current Commercial Projects

## Senior Application Engineer at Oracle
**Orlando, FL** _Feb 2014 to Present_

  - Fusion Cloud - Knowledge Management Product (KM)

    Technologies used: Java, JDeveloper, ADF, WebLogic, Oracle Database.

  - Right Now Cloud - Knowledge Management Product (OKCS)

    Technologies used: Java, Eclipse, EclipseLink ORM, Jersey JAX-RS, WebLogic, MySQL.


## Django Developer at SHIFT Ministry
**Orlando, FL** _Feb 2013 to Present_

  - Eventful web application

    Technologies used: Python 2.7, Django 1.6, Bootstrap, JQuery, SublimeText, MySQL, Mercurial(hg).

  - Simple CRM web application

    Technologies used: Python 3.5, Django 1.9, PyCharm, PostgreSQL, Mercurial(hg).


---
# Open Source Projects


## Quartz Scheduler Project 
<http://quartz-scheduler.org>

I was a committer for Quartz Scheduler project, and I often helped users on their forums. I have fixed many bug issues and implemented new features during the quartz 2.1 release. You may find my commits with `light5` id in their repository.

## MySchedule Project 
<http://code.google.com/p/myschedule>

I have written a UI front-end web application to managing quartz schedulers. I am the owner of this project, and I have done build, release, and written full documentation on how to use the application. There are several thousands of downloads since the first release.

## TimeMachine Scheduler Project
<https://bitbucket.org/timemachine/scheduler>

I’ve created a Java scheduler project that is fully scalable for enterprise use. It’s a scheduler application that has easy job configuration and flexible scheduling with plugable data storage.


## SweetScala Web Framework
<https://code.google.com/archive/p/sweetscala>

Sweet is a MVC based web application framework for building dynamic web applications. The framework is made with Scala, an Object Oriented and Functional programming language that runs on JVM.

