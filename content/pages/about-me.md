Title: About Me

![Zemian Deng](https://lh6.googleusercontent.com/-fquzGSrEtHc/VLw-5hGbf_I/AAAAAAAAApg/PDMgbr6qedYYa1mX6LtbmkvMHRN2efb9ACL0B/s591-no/Zemian.jpg)

Hi, My name is Zemian Deng and I am a software developer.

I use [Java](https://www.oracle.com/java) as my primary programming language, and I have been working in IT field for over 10 years. I also enjoy using other dynamic language such as [Python](https://www.python.org). I use this blog as my personal journal to record tips and notes on what I have learned.

This is a personal weblog. The opinions expressed here represent my own and not those of my employer.

All data and information provided on this site is for informational purposes only. I make no representations as to accuracy, completeness, currentness, suitability, or validity of any information on this site and will not be liable for any errors, omissions, or delays in this information or any losses, injuries, or damages arising from its display or use. All information is provided on an as-is basis.
